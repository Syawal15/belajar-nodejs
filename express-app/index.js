/* file inde.js */

const express = require('express')
const app = express()
const port = 3000

// app.get('/', (req, res) => res.send('Hello World')) // <= tambahkan ini
// app.get('/post/:id', (req, res) => res.send('artikel-' + req.params.id))
// app.get('/foods', (req, res) => {
//     const page = req.query.page ? req.query.page : 1
//     res.write('Foods page: ' + page + '\n')
//     if(req.query.sort) res.write('Sort by :' + req.query.sort)
//     res.end()
// })
// app.get('/page-*', (req, res) => {
//     res.send('route: ' + req.path)
// })
const log = (req, res, next) => {
    console.log(Date.now() + ' ' + req.ip + ' ' + req.originalUrl)
    next()
}
app.use(log)

const routers = require('./routers')
app.use(routers)

const notFound = (req, res, next) => {
    res.json({
        status: 'error',
        message: 'resource tidak ditemuka'
    })
}
app.use(notFound)

app.listen(port, () => console.log(`Server running at http://localhost:${port}`))


