const express = require('express')
const { append } = require('express/lib/response')
const routers = express.Router()

routers.get('/', (req, res) => res.send('Hello World 12'))

routers.get('/post/:id?', (req, res) => {
    if(req.params.id) res.send('artikel-' + req.params.id)
})

module.exports = routers